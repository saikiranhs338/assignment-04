#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@registration
Feature: Registration

  @tc01
  Scenario: Registration Given the url http://spezicoe.wipro.com:81/opencart1/ is launched
Given the url http://spezicoe.wipro.com:81/opencart1/ is launched

When the user clicks on My Account and Register buttons

And the user enters the mandatory fields

And checks the privacy policy checkbox

And clicks the continue button

Then Congratulations! Your new account has been successfully created! message should be displayed

And User should be able to logout

